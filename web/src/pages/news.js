import React from 'react';
import Container from "../components/container";
import SEO from '../components/seo';
import Layout from "../containers/layout";
import ProjectTemplate from '../templates/project';
import { useStaticQuery, graphql } from "gatsby";
import { GatsbyImage } from 'gatsby-plugin-image';
import { getGatsbyImageData } from 'gatsby-source-sanity';
import styled from "styled-components";

export const data = graphql`
    query {
        allSanitySampleProject {
            edges {
              node {
                _rawMainImage
                _createdAt
                _id
                _rawBody
              }
            }
          }
    }
  `;

function News({data}) {
const sanityConfig = {projectId: 'r87vnyrr', dataset: 'production'}
//const project = data && data.sampleProject;
//const data = query.newsQuery;

    return (
        <Layout>
            <SEO title="News"></SEO>
            <Container>
                <h2>News</h2>
                <div>
                    {data && data.allSanitySampleProject.edges.map(x => {
                        return (
                        <NewsBox>
                            {console.log(x)}
                            <p>{x.node._rawMainImage.alt}</p>
                            {console.log(getGatsbyImageData(x.node._rawMainImage.asset._ref, {maxWidth: 300, maxHeight:100}, sanityConfig))}
                            <GatsbyImage alt={x.node._rawMainImage.alt} image={getGatsbyImageData(x.node._rawMainImage.asset._ref, {maxWidth: 1024}, sanityConfig)} />
                        </NewsBox>)
                    })}
                </div>
            </Container>
        </Layout>
    )
}

export default News;

const NewsBox = styled("div")`
max-width: 20em;
display: inline-block;`

const Inner = styled("div")`
margin-bottom: 200px;`
