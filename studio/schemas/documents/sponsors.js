export default {
    name: 'sponsors',
    type: 'document',
    title: 'Sponsors',
    fields: [
      {
        name: 'title',
        type: 'string',
        title: 'Company Name'
      },
      {
        name: 'logo',
        title: 'Logo',
        type: 'figure'
      },
    ]
  }